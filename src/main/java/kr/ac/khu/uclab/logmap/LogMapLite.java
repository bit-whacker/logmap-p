package kr.ac.khu.uclab.logmap;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;

import uk.ac.manchester.syntactic_locality.OntologyModuleExtractor;
import uk.ac.ox.krr.logmap2.io.LogOutput;
import uk.ac.ox.krr.logmap2.owlapi.SynchronizedOWLManager;
import uk.ac.ox.krr.logmap_lite.MappingObjectStr;
import uk.ac.ox.krr.logmap_lite.OntologyLoader;
import uk.ac.ox.krr.logmap_lite.OntologyProcessing;
import uk.ac.ox.krr.logmap_lite.io.ReadFile;

public class LogMapLite {
    private static Logger LOG = LogManager.getRootLogger();

    private OntologyLoader onto_loader1;
    private OntologyLoader onto_loader2;

    private OntologyProcessing onto_proc1;
    private OntologyProcessing onto_proc2;
    
    private OWLOntology module1_overlapping;
    private OWLOntology module2_overlapping;
    private boolean extract_overlapping = false;
    private boolean check_with_gold_standard = false;
    private boolean outPutOnlyIndiv = false;

    private String gs_mappings_file;
    private Map<Integer, Set<Integer>> mappings;
    private Map<Integer, Set<Integer>> mappingsOProp;
    private Map<Integer, Set<Integer>> mappingsDProp;
    private Map<Integer, Set<Integer>> mappingsIndiv;

    // private Set<MappingObjectStr> mappings_ll = new HashSet<MappingObjectStr>();
    // private Set<MappingObjectStr> mappings_gs = new HashSet<MappingObjectStr>();
    private Set<MappingObjectStr> mappings_ll = ConcurrentHashMap.newKeySet();
    private Set<MappingObjectStr> mappings_gs = ConcurrentHashMap.newKeySet();
    
    static final int iCPU = Runtime.getRuntime().availableProcessors();
    ExecutorService executorService;

    public LogMapLite() {
    }

    /**
     * Instantiating LogMapLite with the following parameters
     * <p>
     * 
     * @param iri1_str
     * @param iri2_str
     * @param gs_mappings_file
     * @param create_owl_mappings_onto
     * @param extract_overlapping
     * @param eval_impact
     * @param etype
     */
    public LogMapLite(String iri1_str, 
            String iri2_str,
            String gs_mappings_file, 
            boolean create_owl_mappings_onto,
            boolean extract_overlapping, 
            boolean eval_impact,
            EXECUTION_TYPE etype) {

        this.extract_overlapping = extract_overlapping;
        this.gs_mappings_file = gs_mappings_file;
        
        if (!gs_mappings_file.equals(""))
            check_with_gold_standard = true;

        try {
            long start = System.currentTimeMillis();
            switch (etype) {
            case PARALLEL:
                int i=0;
                
                for(i=0; i<10; i++){
                    long tmp = System.currentTimeMillis();
                    LOG.debug("Parallel Loading...");
                    loadAndProcessParallel(iri1_str, iri2_str);
                    tmp = System.currentTimeMillis() - tmp;
                    System.err.println("iteration " + i + ": " + tmp + " ms]");
                }
                long timeTaken = System.currentTimeMillis() - start;
                timeTaken = (timeTaken/i);
                System.err.println("Average Time: [" + timeTaken + " ms]");
                break;
            case SEQUENTIAL:
                i=0;
                for(i=0; i<10; i++){
                    long tmp = System.currentTimeMillis();
                    LOG.debug("Sequential Loading...");
                    loadAndProcessSequential(iri1_str, iri2_str);
                    tmp = System.currentTimeMillis() - tmp;
                    System.err.println("iteration " + i + ": " + tmp + " ms]");
                }
                timeTaken = System.currentTimeMillis() - start;
                timeTaken = (timeTaken/i);
                System.err.println("Average Time: [" + timeTaken + " ms]");
                break;
            default:
                break;
            }

            LOG.debug("Done!");
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            if(!executorService.isShutdown())
                executorService.shutdownNow();
        }
    }

    /**
     * A parallel ontology loader and processor
     * <p>
     * 
     * @param iri1_str
     * @param iri2_str
     * @throws Exception
     */
    private void loadAndProcessParallel(final String iri1_str, final String iri2_str) throws Exception {
        int threads = 1;// iCPU; //.(noOfThreads <= iCPU)? noOfThreads : iCPU;
        executorService = Executors.newFixedThreadPool(threads);

        /** loading task */
        Future<OntologyLoader> loaderFuture1 = executorService.submit(() -> new OntologyLoader(iri1_str));
        Future<OntologyLoader> loaderFuture2 = executorService.submit(() -> new OntologyLoader(iri2_str));
        onto_loader2 = loaderFuture2.get();
        onto_loader1 = loaderFuture1.get();

        /** processing task -> depends on ontology_loader */
        Future<OntologyProcessing> processFuture1 = executorService.submit(() -> new OntologyProcessing(onto_loader1.getOWLOntology(), extract_overlapping));
        Future<OntologyProcessing> processFuture2 = executorService.submit(() -> new OntologyProcessing(onto_loader2.getOWLOntology(), extract_overlapping));

        onto_proc1 = processFuture1.get();
        onto_proc2 = processFuture2.get();

        /** axiom creation task -> depends on ontology_loader */
        executorService.submit(() -> onto_loader1.createAxiomSet());
        executorService.submit(() -> onto_loader2.createAxiomSet());

        /**
         * cleaning task -> depends on ontology_loader, and ontology_processor.
         * parallelization here doesn't gain much performance
         */
        onto_loader1.clearOntology();
        onto_loader2.clearOntology();
        onto_proc1.clearOntoloy();
        onto_proc2.clearOntoloy();

        /** compute mappings */
        Future<Map<Integer, Set<Integer>>> classMappings = executorService.submit(() -> computeClassMappings());
        Future<Map<Integer, Set<Integer>>> dataPropMappings = executorService.submit(() -> computeDataPropMappings());
        Future<Map<Integer, Set<Integer>>> objectPropMappings = executorService.submit(() -> computeObjPropMappings());
        Future<Map<Integer, Set<Integer>>> individualMappings = executorService.submit(() -> computeIndividualMappings());
        
        mappings = classMappings.get();
        mappingsIndiv = individualMappings.get();
        mappingsDProp = dataPropMappings.get();
        mappingsOProp = objectPropMappings.get();
        List<Future<Boolean>> mapFuture = null;
        /** add all computed mappings to mappings_ll*/
        if (check_with_gold_standard && !outPutOnlyIndiv) {
            /** add class mappings to mappings_ll*/
            mapFuture = Arrays.asList(executorService.submit(()->{
                for (int ide1 : mappings.keySet()) {
                    for (int ide2 : mappings.get(ide1)) {
                        mappings_ll.add(new MappingObjectStr(onto_proc1
                                .getIRI4identifier(ide1), onto_proc2
                                .getIRI4identifier(ide2)));

                    }
                }
                return true;
            }),
            /** add indvidual mappings to mappings_ll*/
            executorService.submit(()->{
                for (int ide1 : mappingsIndiv.keySet()) {
                    for (int ide2 : mappingsIndiv.get(ide1)) {
                        mappings_ll.add(new MappingObjectStr(onto_proc1
                                .getIRI4Individual(ide1), onto_proc2
                                .getIRI4Individual(ide2)));
    
                    }
                }
                return true;
            }),
            /** add dataProperty mappings to mappings_ll*/
            executorService.submit(()->{
                for (int ide1 : mappingsDProp.keySet()) {
                    for (int ide2 : mappingsDProp.get(ide1)) {
                        mappings_ll.add(new MappingObjectStr(onto_proc1
                                .getIRI4DPropIdentifier(ide1), onto_proc2
                                .getIRI4DPropIdentifier(ide2)));
    
                    }
                }
                return true;
            }),
            /** add objectProperty mappings to mappings_ll*/
            executorService.submit(()->{
                for (int ide1 : mappingsOProp.keySet()) {
                    for (int ide2 : mappingsOProp.get(ide1)) {
                        mappings_ll.add(new MappingObjectStr(onto_proc1
                                .getIRI4OPropIdentifier(ide1), onto_proc2
                                .getIRI4OPropIdentifier(ide2)));
    
                    }
                }
                return true;
            })
            );
        }
        while(!(mapFuture.get(0).isDone() && mapFuture.get(1).isDone() && mapFuture.get(2).isDone() && mapFuture.get(3).isDone())){
            LOG.debug("adding mappings please wait...");
            Thread.sleep(50);
        }
        LOG.debug("all mappings have been added to mappings_ll");
        LOG.debug("now computing precision, recall and fmeasure");
        if (check_with_gold_standard){
            /** loads gs_mappings_file */
            loadMappingsGS(gs_mappings_file);
            /** performs arithmetic operations */
            getPrecisionAndRecallMappings();
        }
        
        if(extract_overlapping)
            computeOveralappingParallel();
        
        executorService.shutdownNow();
    }

    /**
     * A sequential ontology loader and processor
     * <p>
     * 
     * @param iri1_str
     * @param iri2_str
     * @throws Exception
     */
    private void loadAndProcessSequential(String iri1_str, String iri2_str) throws Exception {
        /** loading task */
        onto_loader1 = new OntologyLoader(iri1_str);
        onto_loader2 = new OntologyLoader(iri2_str);

        /** processing task -> depends on ontology_loader */
        onto_proc1 = new OntologyProcessing(onto_loader1.getOWLOntology(), extract_overlapping);
        onto_proc2 = new OntologyProcessing(onto_loader2.getOWLOntology(), extract_overlapping);

        /** axiom creation task -> depends on ontology_loader */
        onto_loader1.createAxiomSet(); // we only keep set of axioms
        onto_loader2.createAxiomSet();

        /** cleaning task -> depends on ontology_loader, and ontology_processor */
        onto_loader1.clearOntology();
        onto_loader2.clearOntology();
        onto_proc1.clearOntoloy();
        onto_proc2.clearOntoloy();
        
        
        mappings = computeClassMappings();
        mappingsDProp = computeDataPropMappings();
        mappingsOProp = computeObjPropMappings();
        mappingsIndiv = computeIndividualMappings();
        
        if (check_with_gold_standard && !outPutOnlyIndiv) {
            for (int ide1 : mappings.keySet()) {
                for (int ide2 : mappings.get(ide1)) {
                    mappings_ll.add(new MappingObjectStr(onto_proc1
                            .getIRI4identifier(ide1), onto_proc2
                            .getIRI4identifier(ide2)));
    
                }
            }
            
            for (int ide1 : mappingsDProp.keySet()) {
                for (int ide2 : mappingsDProp.get(ide1)) {
                    mappings_ll.add(new MappingObjectStr(onto_proc1
                            .getIRI4DPropIdentifier(ide1), onto_proc2
                            .getIRI4DPropIdentifier(ide2)));
    
                }
            }
            
            for (int ide1 : mappingsOProp.keySet()) {
                for (int ide2 : mappingsOProp.get(ide1)) {
                    mappings_ll.add(new MappingObjectStr(onto_proc1
                            .getIRI4OPropIdentifier(ide1), onto_proc2
                            .getIRI4OPropIdentifier(ide2)));
    
                }
            }
            
            for (int ide1 : mappingsIndiv.keySet()) {
                for (int ide2 : mappingsIndiv.get(ide1)) {
                    mappings_ll.add(new MappingObjectStr(onto_proc1
                            .getIRI4Individual(ide1), onto_proc2
                            .getIRI4Individual(ide2)));
    
                }
            }
        }
        LOG.debug("all mappings have been added to mappings_ll");
        LOG.debug("now computing precision, recall and fmeasure");
        if (check_with_gold_standard){
            /** loads gs_mappings_file */
            loadMappingsGS(gs_mappings_file);
            /** performs arithmetic operations */
            getPrecisionAndRecallMappings();
        }
        
        if(extract_overlapping)
            computeOveralappingSequential();
    }

    /** computing mappings */
    private Map<Integer, Set<Integer>> computeClassMappings() {

        Set<Set<String>> if_exact_intersect;

        if_exact_intersect = onto_proc1.getInvertedFileExact().keySet();
        if_exact_intersect.retainAll(onto_proc2.getInvertedFileExact().keySet());
        onto_proc2.getInvertedFileExact().keySet().retainAll(if_exact_intersect);

        mappings = new HashMap<Integer, Set<Integer>>();

        for (Set<String> entry : if_exact_intersect) {

            if (entry.isEmpty()) {
                System.out.println("EMPTY SET IN IF CLASSES");
                continue;
            }

            for (int ide1 : onto_proc1.getInvertedFileExact().get(entry)) {

                if (!mappings.containsKey(ide1))
                    mappings.put(ide1, new HashSet<Integer>());

                for (int ide2 : onto_proc2.getInvertedFileExact().get(entry)) {
                    mappings.get(ide1).add(ide2);

                }

            }
        }
        return mappings;
    }

    private Map<Integer, Set<Integer>> computeIndividualMappings() {

        Set<Set<String>> if_exact_intersect_indiv;

        if_exact_intersect_indiv = onto_proc1.getInvertedFileExactIndividuals().keySet();
        if_exact_intersect_indiv.retainAll(onto_proc2.getInvertedFileExactIndividuals().keySet());
        onto_proc2.getInvertedFileExactIndividuals().keySet().retainAll(if_exact_intersect_indiv);

        mappingsIndiv = new HashMap<Integer, Set<Integer>>();

        for (Set<String> entry : if_exact_intersect_indiv) {

            if (entry.isEmpty()) {
                System.out.println("EMPTY SET IN IF CLASSES");
                continue;
            }

            for (int ide1 : onto_proc1.getInvertedFileExactIndividuals().get(entry)) {

                if (!mappingsIndiv.containsKey(ide1))
                    mappingsIndiv.put(ide1, new HashSet<Integer>());

                for (int ide2 : onto_proc2.getInvertedFileExactIndividuals().get(entry)) {
                    mappingsIndiv.get(ide1).add(ide2);

                }

            }
        }
        return mappingsIndiv;
    }

    private Map<Integer, Set<Integer>> computeDataPropMappings() {

        Set<Set<String>> if_exact_intersect;

        if_exact_intersect = onto_proc1.getInvertedFileExactDataProp().keySet();
        if_exact_intersect.retainAll(onto_proc2.getInvertedFileExactDataProp().keySet());
        onto_proc2.getInvertedFileExactDataProp().keySet().retainAll(if_exact_intersect);

        mappingsDProp = new HashMap<Integer, Set<Integer>>();

        for (Set<String> entry : if_exact_intersect) {
            for (int ide1 : onto_proc1.getInvertedFileExactDataProp().get(entry)) {

                if (!mappingsDProp.containsKey(ide1))
                    mappingsDProp.put(ide1, new HashSet<Integer>());

                for (int ide2 : onto_proc2.getInvertedFileExactDataProp().get(entry)) {
                    mappingsDProp.get(ide1).add(ide2);

                }

            }
        }
        return mappingsDProp;
    }

    private Map<Integer, Set<Integer>> computeObjPropMappings() {

        Set<Set<String>> if_exact_intersect;

        if_exact_intersect = onto_proc1.getInvertedFileExactObjectProp().keySet();
        if_exact_intersect.retainAll(onto_proc2.getInvertedFileExactObjectProp().keySet());
        onto_proc2.getInvertedFileExactObjectProp().keySet().retainAll(if_exact_intersect);

        mappingsOProp = new HashMap<Integer, Set<Integer>>();

        for (Set<String> entry : if_exact_intersect) {
            for (int ide1 : onto_proc1.getInvertedFileExactObjectProp().get(entry)) {

                if (!mappingsOProp.containsKey(ide1))
                    mappingsOProp.put(ide1, new HashSet<Integer>());

                for (int ide2 : onto_proc2.getInvertedFileExactObjectProp().get(entry)) {
                    mappingsOProp.get(ide1).add(ide2);

                }

            }
        }
        return mappingsOProp;
    }
    /**
     * Load gold standards
     * @param gs_mappings
     * @throws Exception
     */
    private void loadMappingsGS(String gs_mappings) throws Exception{
        ReadFile reader = new ReadFile(gs_mappings);
        
        String line;
        String[] elements;
        line=reader.readLine();
        
        while (line!=null) {
            if (line.indexOf("|")<0 && line.indexOf("\t")<0){
                line=reader.readLine();
                continue;
            }
            if (line.indexOf("|")>=0)
                elements=line.split("\\|");
            else {
                elements=line.split("\\t");
            }
            
            mappings_gs.add(new MappingObjectStr(elements[0], elements[1]));    
            line=reader.readLine();
        }       
        reader.closeBuffer();
    }
    
    double precision=0.0;
    double recall=0.0;
    double fmeasure=0.0;
    
    public double getPrecision(){
        return precision;
    }
    public double getRecall(){
        return recall;
    }
    public double getFmeasure(){
        return fmeasure;
    }
    /**
     * Calculates precision, recall and fmeasure
     * 
     * @throws Exception
     */
    private void getPrecisionAndRecallMappings() throws Exception{
        Set <MappingObjectStr> intersection;
        //ALL UMLS MAPPINGS
        intersection=new HashSet<MappingObjectStr>(mappings_gs);
        intersection.retainAll(mappings_ll);
        
        precision=((double)intersection.size())/((double)mappings_ll.size());
        recall=((double)intersection.size())/((double)mappings_gs.size());
    
        fmeasure=(2*recall*precision)/(precision+recall);
        
        
        LOG.debug("WRT GS MAPPINGS");
        LOG.debug("\tPrecision Mappings: " + precision);
        LOG.debug("\tRecall Mapping: " + recall);
        LOG.debug("\tF measure: " + (2*recall*precision)/(precision+recall));

        Set <MappingObjectStr> difference;
        difference=new HashSet<MappingObjectStr>(mappings_gs);
        difference.removeAll(mappings_ll);
        LOG.debug("Difference in GS: " + difference.size());
        
        Set <MappingObjectStr> difference2;
        difference2=new HashSet<MappingObjectStr>(mappings_ll);
        difference2.removeAll(mappings_gs);
        LOG.debug("Difference in Candidates: " + difference2.size());
    }
    
    private void computeOveralappingParallel(){
        
        Set<Set<String>> if_weak_intersect;
        
        /** pc_1a */
        if_weak_intersect = onto_proc1.getWeakInvertedFile().keySet();
        if_weak_intersect.retainAll(onto_proc2.getWeakInvertedFile().keySet());
        /** pc_1b */
        onto_proc2.getWeakInvertedFile().keySet().retainAll(if_weak_intersect);
        
        Set<OWLEntity> entities1 = ConcurrentHashMap.newKeySet();
        Set<OWLEntity> entities2 = ConcurrentHashMap.newKeySet();
        
        for (final Set<String> str_set: if_weak_intersect){
            /** pc_2a */
            executorService.submit(()->{
                for (int ide1 : onto_proc1.getWeakInvertedFile().get(str_set)){
                    entities1.add(onto_proc1.getOWLClass4identifier(ide1));
                }
            });
            
            /** pc_2b */
            executorService.submit(()->{
                for (int ide2 : onto_proc2.getWeakInvertedFile().get(str_set)){
                    entities2.add(onto_proc2.getOWLClass4identifier(ide2));
                }  
            });  
        }
        onto_proc1.clearStructures();
        onto_proc2.clearStructures();

        try {
            OntologyModuleExtractor module_extractor1 =
                    new OntologyModuleExtractor(SynchronizedOWLManager.createOWLOntologyManager(), onto_loader1.getAxiomSet(), true, false, true);
            Future<OWLOntology> futureOWL1 = executorService.submit(()-> module_extractor1.extractAsOntology(entities1, IRI.create(onto_loader1.getOntologyIRIStr())));
            
            OntologyModuleExtractor module_extractor2 =     
                    new OntologyModuleExtractor( SynchronizedOWLManager.createOWLOntologyManager(), onto_loader2.getAxiomSet(), true, false, true);
            Future<OWLOntology> futureOWL2 = executorService.submit(()-> module_extractor2.extractAsOntology(entities2, IRI.create(onto_loader2.getOntologyIRIStr())));
            //. module1_overlapping = module_extractor1.extractAsOntology(entities1, IRI.create(onto_loader1.getOntologyIRIStr()));
            
            module1_overlapping = futureOWL1.get();
            module_extractor1.clearStrutures();
            onto_loader1.clearAxiomSet();
            entities1.clear(); 
            
            module2_overlapping = futureOWL2.get();
            module_extractor2.clearStrutures();     
            onto_loader2.clearAxiomSet();
            entities2.clear();
            
        } catch (InterruptedException | ExecutionException e) {
            LogOutput.print("Error when creating module ontology 1");
            e.printStackTrace();
        } 
    }
    
    private void computeOveralappingSequential(){
        
        Set<Set<String>> if_weak_intersect;
        
        /** pc_1a */
        if_weak_intersect = onto_proc1.getWeakInvertedFile().keySet();
        if_weak_intersect.retainAll(onto_proc2.getWeakInvertedFile().keySet());
        /** pc_1b */
        onto_proc2.getWeakInvertedFile().keySet().retainAll(if_weak_intersect);
        
        
        Set<OWLEntity> entities1 = new HashSet<OWLEntity>();
        Set<OWLEntity> entities2 = new HashSet<OWLEntity>();
        
        for (Set<String> str_set: if_weak_intersect){
            /** pc_2a */
            for (int ide1 : onto_proc1.getWeakInvertedFile().get(str_set)){
                entities1.add(onto_proc1.getOWLClass4identifier(ide1));
            }
            /** pc_2b */
            for (int ide2 : onto_proc2.getWeakInvertedFile().get(str_set)){
                entities2.add(onto_proc2.getOWLClass4identifier(ide2));
            }   
        }
        onto_proc1.clearStructures();
        onto_proc2.clearStructures();
        
        OntologyModuleExtractor module_extractor1 =
                new OntologyModuleExtractor(SynchronizedOWLManager.createOWLOntologyManager(), onto_loader1.getAxiomSet(), true, false, true);
        try {
            module1_overlapping = module_extractor1.extractAsOntology(entities1, IRI.create(onto_loader1.getOntologyIRIStr()));
        } catch (OWLOntologyCreationException e) {
            LogOutput.print("Error when creating module ontology 1");
            //e.printStackTrace();
        }
        
        
        module_extractor1.clearStrutures();
        
        
        onto_loader1.clearAxiomSet();
        entities1.clear(); 
        
        OntologyModuleExtractor module_extractor2 =     
                new OntologyModuleExtractor( SynchronizedOWLManager.createOWLOntologyManager(), onto_loader2.getAxiomSet(), true, false, true);
        try {
            module2_overlapping = module_extractor2.extractAsOntology(entities2, IRI.create(onto_loader2.getOntologyIRIStr()));
        } catch (OWLOntologyCreationException e) {
            LogOutput.print("Error when creating module ontology 2");
            //e.printStackTrace();
        }

        module_extractor2.clearStrutures();     
        onto_loader2.clearAxiomSet();
        entities2.clear();
        
    }
    public static void main(String[] args) {
        LOG.debug("A Parallel Implementation Of LogMap!");

        boolean overlapping = true;
        boolean create_owl_mappings_onto = false;
        boolean eval_impact = false;

         /** small datasets
         String iri1_str = "file:///D:/projects/logMap/dataset/test_data/oaei2012_FMA_small_overlapping_nci.owl";
         String iri2_str = "file:///D:/projects/logMap/dataset/test_data/oaei2012_NCI_small_overlapping_fma.owl";*/
       
        
        /*String iri1_str = "file:///D:/projects/logMap/dataset/test_data/oaei2012_SNOMED_small_overlapping_nci.owl";
        String iri2_str = "file:///D:/projects/logMap/dataset/test_data/oaei2012_NCI_small_overlapping_snomed.owl";
        String gs_mappings_file = "D:/projects/logMap/dataset/gold_standard/oaei2012_SNOMED2NCI_voted_mappings2.0.txt";*/
        
        /** large datasets*/
        String iri1_str = "file:///D:/projects/logMap/dataset/test_data/oaei2012_FMA_whole_ontology.owl";
        String iri2_str = "file:///projects/logMap/dataset/test_data/oaei2012_NCI_whole_ontology.owl";
        
        String gs_mappings_file = "D:/projects/logMap/dataset/gold_standard/oaei2012_FMA2NCI_voted_mappings.txt";
        
        LogMapLite logmap = new LogMapLite(iri1_str, iri2_str,
                gs_mappings_file, create_owl_mappings_onto, overlapping,
                eval_impact, EXECUTION_TYPE.PARALLEL);
    }

    enum EXECUTION_TYPE {
        SEQUENTIAL, PARALLEL;
    };
}
