package kr.ac.khu.uclab.logmap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class TestRunner {
	private static Logger LOG = LogManager.getLogger(TestRunner.class);
	
	public static void main(String[] args) {
	      Result result = JUnitCore.runClasses(TestRunner.class);

	      for (Failure failure : result.getFailures()) {
	         LOG.debug(failure.toString());
	      }
	      LOG.debug(result.wasSuccessful());
	   }
}
