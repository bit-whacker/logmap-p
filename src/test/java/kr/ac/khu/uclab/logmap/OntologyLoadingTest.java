package kr.ac.khu.uclab.logmap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import uk.ac.ox.krr.logmap_lite.OntologyLoader;
/**
 * Unit test for simple App.
 */
public class OntologyLoadingTest {
	private static Logger LOG = LogManager.getLogger(OntologyLoadingTest.class);
	
	static final int iCPU = Runtime.getRuntime().availableProcessors();
    ExecutorService executorService;
    /** large datasets*/ 
    static final String iri1_str = "file:///D:/khu/uclab/projects/logMap/dataset/test_data/oaei2012_FMA_small_overlapping_nci.owl";
    static final String iri2_str = "file:///D:/khu/uclab/projects/logMap/dataset/test_data/oaei2012_NCI_small_overlapping_fma.owl";
    static final String gs_mappings_file = "D:/khu/uclab/projects/logMap/dataset/gold_standard/oaei2012_FMA2NCI_voted_mappings.txt";

    @Test
    public void loadingTest() throws Exception{
    	int threads = iCPU; //.(noOfThreads <= iCPU)? noOfThreads : iCPU;
    	executorService = Executors.newFixedThreadPool(threads);
    	
    	/** sequential loading task */
		OntologyLoader seqLoader1 = new OntologyLoader(iri1_str);
		OntologyLoader seqLoader2 = new OntologyLoader(iri2_str);
		
		/** paralllel loading task */
		Future<OntologyLoader> loaderFuture1 = executorService.submit(()-> new OntologyLoader(iri1_str));
		Future<OntologyLoader> loaderFuture2 = executorService.submit(()-> new OntologyLoader(iri2_str));
		OntologyLoader parallelLoader1 = loaderFuture1.get();
		OntologyLoader parallelLoader2 = loaderFuture2.get();
		
		executorService.shutdownNow();
		
		
		/** test loaded first ontology */
		assertTrue(seqLoader1.getSignatureSize() == parallelLoader1.getSignatureSize());
		assertTrue(seqLoader1.getClassesInSignatureSize() == parallelLoader1.getClassesInSignatureSize());
		assertEquals(seqLoader1.getClassesInSignature(), parallelLoader1.getClassesInSignature());
		
		/** test loaded second ontology */
		assertTrue(seqLoader2.getSignatureSize() == parallelLoader2.getSignatureSize());
		assertTrue(seqLoader2.getClassesInSignatureSize() == parallelLoader2.getClassesInSignatureSize());
		assertEquals(seqLoader2.getClassesInSignature(), parallelLoader2.getClassesInSignature());
	
    }
    
}
