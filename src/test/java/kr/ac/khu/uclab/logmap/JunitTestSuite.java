package kr.ac.khu.uclab.logmap;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
	OntologyLoadingTest.class
})
public class JunitTestSuite {
}
